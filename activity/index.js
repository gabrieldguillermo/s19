// console.log("hello");


// S19 - Javascript - Selection Control Structures

/*ACTIVITY*/


/*

	1. Declare 3 global variables without initialization called username,password and role.
	2. Create a login function which is able to prompt the user to provide their username, password and role.
		a.) use prompt() and update the username,password and role global variables with the prompt() returned values.
		b.) add an if statement to check if the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
			** if it is, show an alert to inform the user that their input should not be empty.
		c.) Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
			** if the user's role is admin, show an alert with the following message:
						"Welcome back to the class portal, admin!"
			** if the user's role is teacher, show an alert with the following message:
						"Thank you for logging in, teacher!"
			** if the user's role is a student, show an alert with the following message:
 						"Welcome to the class portal, student!"
			** if the user's role does not fall under any of the cases, as a default, show a message:
 						"Role out of range."
*/


	// Code here:

	let username;
	let password;
	let role;


	function login(){
		username = prompt("Enter your Username:");
		
			if(!username){
				alert("Username should not be empty!");
			};

		password = prompt("Enter your Password:");
	
			if(!password){
				alert("Password should not be empty!");
			};

		role = prompt("What is your Role?");
		
			if(!role){
				alert("Role should not be empty!");
			} else {

					switch(role){
						case "admin":
							alert("Welcome back to the class portal, admin!");
						break;

						case "teacher":
							alert("Thank you for logging in, teacher!");
						break;

						case "student":
							alert("Welcome to the class portal, student!");
						break;

						default:
							alert("Role out of range.");
						break;
					};
			};
	};

login();



/*
	3. Create a function which is able to receive 4 numbers as arguments, calculate its average and log a message for  the user about their letter equivalent in the console.
		a.) add parameters appropriate to describe the arguments.
		b.)create a new function scoped variable called average.
		c.) calculate the average of the 4 number inputs and store it in the variable average.
		d.)research the use of Math.round() and round off the value of the average variable.
			**update the average variable with the use of Math.round()
			**console.log() the average variable to check if it is rounding off first.

	4. add an if statement to check if the value of avg is less than or equal to 74.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is F"

	5. add an else if statement to check if the value of avg is greater than or equal to 75 and if average is less than or equal to 79.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is D"

	6. add an else if statement to check if the value of avg is greater than or equal to 80 and if average is less than or equal to 84.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is C"

	7. add an else if statement to check if the value of avg is greater than or equal to 85 and if average is less than or equal to 89.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is B"

	8. add an else if statement to check if the value of avg is greater than or equal to 90 and if average is less than or equal to 95.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A"

	9. add an else if statement to check if the value of average is greater than or  equal 96.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A+"

*/


	// Code here:
		
		
	let grades=[89, 83, 81, 93];
	
	function checkAverage(grades){
		let gradesLength = grades.length;
		let totalGrade = 0;

		for(let g of grades){
			totalGrade+=g;
		}

		let getAverage =Math.round(totalGrade / gradesLength); 

		console.log(`Students grades are: ${grades}`);

		if (getAverage < 74 ){
			return `Hello, student, your average is ${getAverage}. The letter equivalent is F`;

		} else if (getAverage >= 75 && getAverage <= 79){
			return `Hello, student, your average is ${getAverage}. The letter equivalent is D`;
		
		} else if (getAverage >= 80 && getAverage <= 84){
			return `Hello, student, your average is ${getAverage}. The letter equivalent is C`;

		} else if (getAverage >= 85 && getAverage <= 89){
			return `Hello, student, your average is ${getAverage}. The letter equivalent is B`;

		} else if (getAverage >= 90 && getAverage <= 95){
		return `Hello, student, your average is ${getAverage}. The letter equivalent is A`;

		}else if ( getAverage >= 96 ){
			return `Hello, student, your average is ${getAverage}. The letter equivalent is A+`;

		};
	};

	const average =  checkAverage(grades);
	console.log(average);